'use strict';

let app=require('express')();
let http=require('http').Server(app);
let io=require('socket.io')(http);

// when a client connects to a socket
io.on('connection',function(socket){
	console.log('user connected');
// when a  client disconnects from a socket
	socket.on('disconnect',function(){
		console.log('user disconnected');
	});
// listening to the msg event raised by client	
	socket.on('msg',function(msg){
	//Any one method can be used to emit event in mentioned below but do read below comments before commenting or uncommenting	
//recieve msg event broadcasted by server to all users available except the sender user	
		  socket.broadcast.emit('recieve msg',msg);
//Recieve msg event raised by server to all users available
		//io.emit('recieve msg',msg);
		
	});

});

app.get('/',function(req,res){
	res.sendFile(__dirname+'/main.htm');
});

http.listen(3000,function(){
	console.log('Server started at port 3000');
});



